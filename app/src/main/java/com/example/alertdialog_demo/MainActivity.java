package com.example.alertdialog_demo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;



public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
           // activity 内部调用退出弹窗
        findViewById(R.id.mainbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });
        // sdk 对外接口调用退出弹窗
        findViewById(R.id.sdk_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SySdk.getInstance().exit(MainActivity.this);
            }
        });
    }


    protected void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("确定要退出吗?");
        builder.setTitle("提示");
        builder.setPositiveButton("确认",

                new android.content.DialogInterface.OnClickListener() {

                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            finish();
                            System.exit(0);
                            android.os.Process.killProcess(android.os.Process.myPid());
                           }
                        catch (Exception ex)
                        {
                            Log.i("error",ex.getMessage());
                        }
                    }

                });
        builder.setNegativeButton("取消",
                new android.content.DialogInterface.OnClickListener() {

                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }

                });

        builder.create().show();

    }
}
