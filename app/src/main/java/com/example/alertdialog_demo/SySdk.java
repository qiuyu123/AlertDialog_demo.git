package com.example.alertdialog_demo;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;
import androidx.appcompat.app.AlertDialog;


/***
 *
 *  创建人：xuqing
 *  创建时间：2020年7月27日13:52:37
 *  类说明：sdk对接接口逻辑
 *
 */



public class SySdk {
    private static SySdk instance = null;
    private  SySdk(){

    }

    public   static   SySdk  getInstance(){
        if(instance==null){
            synchronized (SySdk.class){
                if(instance==null){
                    instance=new SySdk();
                }
            }
        }
        return  instance;
    }

    public  void  exit(final  Activity context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("确定要退出吗?");
        builder.setTitle("提示");
        builder.setPositiveButton("确认",

                new android.content.DialogInterface.OnClickListener() {

                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            context.finish();
                            System.exit(0);
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                        catch (Exception ex)
                        {
                            Log.i("error",ex.getMessage());
                        }
                    }

                });
        builder.setNegativeButton("取消",
                new android.content.DialogInterface.OnClickListener() {

                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });

        builder.create().show();

    }



}
